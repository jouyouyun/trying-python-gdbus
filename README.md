Just some code to try using the Gio DBus interface from Python.

The subdir "server" is my Python version of some example code from the internet.
This uses the "low-level" interfaces. I don't yet understand the "high-level"
interface well enough to know if they're broken or I'm not using them right.

The example-server.c file I found on the internet, and fixed to work with
the current released code. My dbus-example-server.py responds the same way it
does, so that is my claim that I've done it right.

There is one example client in that directory

TODO: more example clients

The "polkit" subdir is where I'm porting to Python an example I found of using PolKit to authorize a client with a server. It uses Gtk.LockButton.

TODO: the subdir "animal" is where I'm trying the "high-level" interfaces. They
don't work because of bugs

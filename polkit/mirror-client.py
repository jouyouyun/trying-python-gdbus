#!/usr/bin/env python3

from gi import require_version as gi_require_version
gi_require_version('Gtk', '3.0')
gi_require_version('Polkit', '1.0')

from gi.repository import Gtk, Gio, Polkit

from mirror_common import BUS_NAME, OBJECT_PATH, INTERFACE_NAME, \
 dump_args, INTERFACE_ACTION

proxy = Gio.DBusProxy.new_for_bus_sync(Gio.BusType.SYSTEM,
                                       Gio.DBusProxyFlags.NONE,
                                       None,
                                       BUS_NAME,
                                       OBJECT_PATH,
                                       INTERFACE_NAME)
print(proxy)

window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
window.connect('delete-event', lambda x, y: Gtk.main_quit())

grid = Gtk.Grid(orientation='vertical')
window.add(grid)

label = Gtk.Label('Text to send:')
grid.add(label)

# We enable the entry field after we've been authorized.
# If you enable it to start, you can enter text and the server will
# ask polkit to authorize the client, then complete the method call as
# normal. The LockButton will reflect the authorized state.
entry = Gtk.Entry(sensitive=False)

error_label = Gtk.Label()
error_label.set_line_wrap(True)

@dump_args
def error_handler(proxy, error, user_data):
    error_label.set_text(str(error))

@dump_args
def result_handler(proxy, result, user_data):
    entry.set_text(result)
    entry.set_sensitive(True)

def mirror_cb(widget):
    widget.set_sensitive(False)
    error_label.set_text('')
    # Unlike  other methods that create a variant for you, the proxy
    # method calls assume you have an outer () pair. They will combine
    # the rest of the calling args into a tuple.
    # *Do* include the enclosing () in the signature
    # *Don't* combine the top-level parameters into a tuple; the
    # lower-level code does that automatically.
    #
    # You can't have an error_handler unless you also have a
    # result_handler. You can have just a result_handler, and any
    # error will be passed to it as the result parameter.
    #
    # user_data, flags, and timeout can also be specified as keywords.
    result = proxy.Reflect('(s)', widget.get_text(),
                           result_handler=result_handler,
                           error_handler=error_handler)

entry.connect('activate', mirror_cb)
grid.add(entry)

grid.add(error_label)

@dump_args
def authorization_changed(_permisson, param):
    entry.set_sensitive(bool(param))

permission = Polkit.Permission.new_sync(INTERFACE_ACTION)
permission.connect('notify', authorization_changed)

button = Gtk.LockButton.new(permission)
button.set_focus_on_click(False)
grid.add(button)

window.show_all()

Gtk.main()


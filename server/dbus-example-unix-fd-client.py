#!/usr/bin/env python3

# Copyright (C) 2015 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <streeter@redhat.com>

from gi.repository import GObject, Gio, GLib

from common import BUS_NAME, dump_args, INTERFACE_NAME, OBJECT_PATH
import sys, os, time

# /glib/gio/tests/gdbus-example-unix-fd-client.c


main_loop = GObject.MainLoop()

def get_server_stdout(connection, name_owner):
    method_call_message = Gio.DBusMessage.new_method_call(name_owner,
                                                          OBJECT_PATH,
                                                          INTERFACE_NAME,
                                                          'GimmeStdout')
    method_reply_message, out_serial = connection.send_message_with_reply_sync(method_call_message,
                                                                               Gio.DBusSendMessageFlags.NONE,
                                                                               -1, None)
    print(method_reply_message, out_serial)
    try:
        # Raises an exception if there is an error. Does nothing otherwise
        method_reply_message.to_gerror()
    except GLib.Error as err:
        # can I do something useful with this?
        #print(method_reply_message.get_error_name())
        raise err
    fd_list = method_reply_message.get_unix_fd_list()
    fd = fd_list.get(0)
    return fd

@dump_args
def name_appeared_handler(connection, name, name_owner):
    fd = get_server_stdout(connection, name_owner)

    msg = 'On {}, dbus-example-unix-fd-client.py with pid {} was here!'
    msg = msg.format(time.strftime('%c'), os.getpid())

    with open(fd, mode='w') as server_stdout:
        print(msg, file=server_stdout)

    print("Wrote the following on server's stdout:")
    print(msg)

    main_loop.quit()

@dump_args
def name_vanished_handler(connection, name):
    print('Failed to get name owner for', name,
          'Is ./dbus-example-server.py running?',
          file=sys.stderr)
    main_loop.quit()

watcher_id = Gio.bus_watch_name(Gio.BusType.SESSION, BUS_NAME,
                                Gio.BusNameWatcherFlags.NONE,
                                name_appeared_handler,
                                name_vanished_handler)

main_loop.run()

Gio.bus_unwatch_name(watcher_id)

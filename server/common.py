OBJECT_PATH = '/org/gtk/GDBus/TestObject'
BUS_NAME = 'org.gtk.GDBus.TestServer'
INTERFACE_NAME = 'org.gtk.GDBus.TestInterface'

def dump_args(func):
    """
    Using for debugging...
    """
    def wrapper(*func_args, **func_kwargs):
        arg_names = func.__code__.co_varnames[:func.__code__.co_argcount]
        args = func_args[:len(arg_names)]
        defaults = func.__defaults__ or ()
        args = args + defaults[len(defaults) -
                               (func.__code__.co_argcount - len(args)):]
        params = list(zip(arg_names, args))
        args = func_args[len(arg_names):]
        if args:
            params.append(('args', args))
        if func_kwargs:
            params.append(('kwargs', func_kwargs))
        print(func.__name__ +
              ' (' + ', '.join('%s = %r' % p for p in params) + ' )')
        return func(*func_args, **func_kwargs)
    return wrapper

#!/usr/bin/env sh

# Copyright (C) 2015 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <streeter@redhat.com>

dbus-send --session --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.gtk.GDBus.TestInterface.GimmeStdout

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.gtk.GDBus.TestInterface.HelloWorld \
  string:'Return Unregistered'


dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.gtk.GDBus.TestInterface.HelloWorld \
  string:'Return Raw'
  
dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.gtk.GDBus.TestInterface.HelloWorld \
  string:'neenerneener'

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.GetAll \
  string:'org.gtk.GDBus.TestInterface'

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.Get \
  string:'org.gtk.GDBus.TestInterface' string:'FluxCapacitorName'

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.Get \
  string:'org.gtk.GDBus.TestInterface' string:'Foo'

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.Get \
  string:'org.gtk.GDBus.TestInterface' string:'Bar'

sleep 2 # "Bar" should change

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.Get \
  string:'org.gtk.GDBus.TestInterface' string:'Bar'

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.Get \
  string:'org.gtk.GDBus.TestInterface' string:'ReadingAlwaysThrowsError'

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.Get \
  string:'org.gtk.GDBus.TestInterface' string:'OnlyWritable'

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.Set \
  string:'org.gtk.GDBus.TestInterface' string:'OnlyWritable' variant:string:'Sup.'

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.Set \
  string:'org.gtk.GDBus.TestInterface' string:'Title' variant:string:'Wat'

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.Get \
  string:'org.gtk.GDBus.TestInterface' string:'Title'

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.Set \
  string:'org.gtk.GDBus.TestInterface' string:'WritingAlwaysThrowsError' \
  variant:string:'boo'

dbus-send --print-reply --dest=org.gtk.GDBus.TestServer \
  /org/gtk/GDBus/TestObject org.freedesktop.DBus.Properties.Set \
  string:'org.gtk.GDBus.TestInterface' string:'Bar' variant:string:'Bar?'

